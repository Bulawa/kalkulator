﻿namespace KalkulatorBMR
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Płeć = new System.Windows.Forms.GroupBox();
            this.rbKobieta = new System.Windows.Forms.RadioButton();
            this.rbMezczyzna = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbDzienne = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbIloscDni = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSrednia = new System.Windows.Forms.RadioButton();
            this.rbWysoka = new System.Windows.Forms.RadioButton();
            this.rbNiska = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb7 = new System.Windows.Forms.RadioButton();
            this.rb4 = new System.Windows.Forms.RadioButton();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.bPrzelicz = new System.Windows.Forms.Button();
            this.tbWzrost = new System.Windows.Forms.TextBox();
            this.tbWiek = new System.Windows.Forms.TextBox();
            this.tbWaga = new System.Windows.Forms.TextBox();
            this.tbWagaDocelowa = new System.Windows.Forms.TextBox();
            this.Płeć.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(20, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wzrost (cm)";
            // 
            // Płeć
            // 
            this.Płeć.Controls.Add(this.rbKobieta);
            this.Płeć.Controls.Add(this.rbMezczyzna);
            this.Płeć.Location = new System.Drawing.Point(36, 157);
            this.Płeć.Name = "Płeć";
            this.Płeć.Size = new System.Drawing.Size(181, 104);
            this.Płeć.TabIndex = 3;
            this.Płeć.TabStop = false;
            this.Płeć.Text = "Płeć";
            this.Płeć.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // rbKobieta
            // 
            this.rbKobieta.AutoSize = true;
            this.rbKobieta.Location = new System.Drawing.Point(20, 54);
            this.rbKobieta.Name = "rbKobieta";
            this.rbKobieta.Size = new System.Drawing.Size(61, 17);
            this.rbKobieta.TabIndex = 1;
            this.rbKobieta.Text = "Kobieta";
            this.rbKobieta.UseVisualStyleBackColor = true;
            // 
            // rbMezczyzna
            // 
            this.rbMezczyzna.AutoSize = true;
            this.rbMezczyzna.Checked = true;
            this.rbMezczyzna.Location = new System.Drawing.Point(20, 32);
            this.rbMezczyzna.Name = "rbMezczyzna";
            this.rbMezczyzna.Size = new System.Drawing.Size(78, 17);
            this.rbMezczyzna.TabIndex = 0;
            this.rbMezczyzna.TabStop = true;
            this.rbMezczyzna.Text = "Mężczyzna";
            this.rbMezczyzna.UseVisualStyleBackColor = true;
            this.rbMezczyzna.CheckedChanged += new System.EventHandler(this.rbMezczyzna_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(70, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Wiek";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(63, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Waga";
            // 
            // tbDzienne
            // 
            this.tbDzienne.Location = new System.Drawing.Point(614, 337);
            this.tbDzienne.Name = "tbDzienne";
            this.tbDzienne.ReadOnly = true;
            this.tbDzienne.Size = new System.Drawing.Size(100, 20);
            this.tbDzienne.TabIndex = 9;
            this.tbDzienne.TextChanged += new System.EventHandler(this.tbDzienne_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(346, 337);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(262, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Twoje dzienne zapotrzebowanie kcl:";
            // 
            // tbIloscDni
            // 
            this.tbIloscDni.Location = new System.Drawing.Point(614, 367);
            this.tbIloscDni.Name = "tbIloscDni";
            this.tbIloscDni.ReadOnly = true;
            this.tbIloscDni.Size = new System.Drawing.Size(100, 20);
            this.tbIloscDni.TabIndex = 11;
            this.tbIloscDni.TextChanged += new System.EventHandler(this.tbIloscDni_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(403, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ilość dni do osiągnięcia celu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(247, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Waga docelowa";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbSrednia);
            this.groupBox1.Controls.Add(this.rbWysoka);
            this.groupBox1.Controls.Add(this.rbNiska);
            this.groupBox1.Location = new System.Drawing.Point(265, 157);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 104);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Aktywność w ciągu dnia";
            // 
            // rbSrednia
            // 
            this.rbSrednia.AutoSize = true;
            this.rbSrednia.Location = new System.Drawing.Point(20, 54);
            this.rbSrednia.Name = "rbSrednia";
            this.rbSrednia.Size = new System.Drawing.Size(61, 17);
            this.rbSrednia.TabIndex = 2;
            this.rbSrednia.Text = "Średnia";
            this.rbSrednia.UseVisualStyleBackColor = true;
            // 
            // rbWysoka
            // 
            this.rbWysoka.AutoSize = true;
            this.rbWysoka.Location = new System.Drawing.Point(20, 76);
            this.rbWysoka.Name = "rbWysoka";
            this.rbWysoka.Size = new System.Drawing.Size(64, 17);
            this.rbWysoka.TabIndex = 1;
            this.rbWysoka.Text = "Wysoka";
            this.rbWysoka.UseVisualStyleBackColor = true;
            // 
            // rbNiska
            // 
            this.rbNiska.AutoSize = true;
            this.rbNiska.Checked = true;
            this.rbNiska.Location = new System.Drawing.Point(20, 32);
            this.rbNiska.Name = "rbNiska";
            this.rbNiska.Size = new System.Drawing.Size(52, 17);
            this.rbNiska.TabIndex = 0;
            this.rbNiska.TabStop = true;
            this.rbNiska.Text = "Niska";
            this.rbNiska.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb7);
            this.groupBox2.Controls.Add(this.rb4);
            this.groupBox2.Controls.Add(this.rb3);
            this.groupBox2.Location = new System.Drawing.Point(533, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(181, 104);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ilość treningów w tygodniu";
            // 
            // rb7
            // 
            this.rb7.AutoSize = true;
            this.rb7.Location = new System.Drawing.Point(20, 76);
            this.rb7.Name = "rb7";
            this.rb7.Size = new System.Drawing.Size(49, 17);
            this.rb7.TabIndex = 2;
            this.rb7.Text = "5 - 7 ";
            this.rb7.UseVisualStyleBackColor = true;
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Location = new System.Drawing.Point(20, 54);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(46, 17);
            this.rb4.TabIndex = 1;
            this.rb4.Text = "3 - 5";
            this.rb4.UseVisualStyleBackColor = true;
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Checked = true;
            this.rb3.Location = new System.Drawing.Point(20, 32);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(46, 17);
            this.rb3.TabIndex = 0;
            this.rb3.TabStop = true;
            this.rb3.Text = "1 - 3";
            this.rb3.UseVisualStyleBackColor = true;
            // 
            // bPrzelicz
            // 
            this.bPrzelicz.Location = new System.Drawing.Point(36, 302);
            this.bPrzelicz.Name = "bPrzelicz";
            this.bPrzelicz.Size = new System.Drawing.Size(271, 93);
            this.bPrzelicz.TabIndex = 18;
            this.bPrzelicz.Text = "Przelicz";
            this.bPrzelicz.UseVisualStyleBackColor = true;
            this.bPrzelicz.Click += new System.EventHandler(this.bPrzelicz_Click);
            // 
            // tbWzrost
            // 
            this.tbWzrost.Location = new System.Drawing.Point(121, 39);
            this.tbWzrost.Name = "tbWzrost";
            this.tbWzrost.Size = new System.Drawing.Size(100, 20);
            this.tbWzrost.TabIndex = 19;
            // 
            // tbWiek
            // 
            this.tbWiek.Location = new System.Drawing.Point(121, 72);
            this.tbWiek.Name = "tbWiek";
            this.tbWiek.Size = new System.Drawing.Size(100, 20);
            this.tbWiek.TabIndex = 20;
            // 
            // tbWaga
            // 
            this.tbWaga.Location = new System.Drawing.Point(121, 105);
            this.tbWaga.Name = "tbWaga";
            this.tbWaga.Size = new System.Drawing.Size(100, 20);
            this.tbWaga.TabIndex = 21;
            // 
            // tbWagaDocelowa
            // 
            this.tbWagaDocelowa.Location = new System.Drawing.Point(375, 105);
            this.tbWagaDocelowa.Name = "tbWagaDocelowa";
            this.tbWagaDocelowa.Size = new System.Drawing.Size(100, 20);
            this.tbWagaDocelowa.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbWagaDocelowa);
            this.Controls.Add(this.tbWaga);
            this.Controls.Add(this.tbWiek);
            this.Controls.Add(this.tbWzrost);
            this.Controls.Add(this.bPrzelicz);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbIloscDni);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbDzienne);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Płeć);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Płeć.ResumeLayout(false);
            this.Płeć.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox Płeć;
        private System.Windows.Forms.RadioButton rbKobieta;
        private System.Windows.Forms.RadioButton rbMezczyzna;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbDzienne;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbIloscDni;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSrednia;
        private System.Windows.Forms.RadioButton rbWysoka;
        private System.Windows.Forms.RadioButton rbNiska;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb7;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.Button bPrzelicz;
        private System.Windows.Forms.TextBox tbWzrost;
        private System.Windows.Forms.TextBox tbWiek;
        private System.Windows.Forms.TextBox tbWaga;
        private System.Windows.Forms.TextBox tbWagaDocelowa;
        private System.Windows.Forms.RadioButton rb4;
    }
}

