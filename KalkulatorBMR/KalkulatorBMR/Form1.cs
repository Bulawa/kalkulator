﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KalkulatorBMR
{
    

    public partial class Form1 : Form
    {
        string sWzrost, sWiek, sWaga, sWagaDocelowa, sDzienneZapotrzebowanie;
        decimal wskaznik, wynik, wzrost, wiek, waga, wagadocelowa, zapotrzebowanie_podstawowe;

        public Form1()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

     /// <summary>
     /// metoda spraedzajaca czy został zaznaczony checkbox meżczyzna
     /// </summary>
     /// <param name="wzrost"></param>
     /// <param name="waga"></param>
     /// <param name="wiek"></param>
        public void Mezczyzna(decimal wzrost, decimal waga, decimal wiek)
        {
            wynik = 66 + (13.7m * waga) + (5 * wzrost) - (6.76m * wiek);
            zapotrzebowanie_podstawowe = wynik;
        }

        public decimal Mezczyzna1(decimal wzrost, decimal waga, decimal wiek)
        {
            wynik = 66 + (13.7m * waga) + (5 * wzrost) - (6.76m * wiek);
            return wynik;
        }




        /// <summary>
        ///  metoda spraedzajaca czy został zaznaczony checkbox kobieta
        /// </summary>
        /// <param name="wzrost"></param>
        /// <param name="waga"></param>
        /// <param name="wiek"></param>
        public void Kobieta(decimal wzrost, decimal waga, decimal wiek)
        {
            wynik = 655 + (9.6m * waga) + (1.8m * wzrost) - (4.7m * wiek);
            zapotrzebowanie_podstawowe = wynik;
        }


        /// <summary>
        /// Metoda obliczająca wynik
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bPrzelicz_Click(object sender, EventArgs e)
        {
            sWaga = tbWaga.Text;
            if (sWaga != "")
            {
                waga = Convert.ToDecimal(sWaga);
            }
            else
            {
                MessageBox.Show("Nie podano wagi!");
                return;
            }

            sWiek = tbWiek.Text;
            if (sWiek != "")
            {
                wiek = Convert.ToDecimal(sWiek);
            }
            else
            {
                MessageBox.Show("Nie podano wieku!");
                return;
            }

            sWzrost = tbWzrost.Text;
            if (sWzrost != "")
            {
                wzrost = Convert.ToDecimal(sWzrost);
            }
            else
            {
                MessageBox.Show("Nie podano wzrostu");
                return;
            }

            if (rbMezczyzna.Checked)
            {
                Mezczyzna(wzrost, waga, wiek);
            }
            else
            {
                Kobieta(wzrost, waga, wiek);
            }


            if (rbNiska.Checked & rb3.Checked) 
            {
                wynik = wynik * 1.2m;
            }
            else if (rbNiska.Checked & rb4.Checked)
            {
                wynik = wynik * 1.3m;
            }
            else if (rbNiska.Checked & rb7.Checked)
            {
                wynik = wynik * 1.4m;
            }
            else if (rbSrednia.Checked & rb3.Checked)
            {
                wynik = wynik * 1.5m;
            }
            else if (rbSrednia.Checked & rb4.Checked)
            {
                wynik = wynik * 1.6m;
            }
            else if (rbSrednia.Checked & rb7.Checked)
            {
                wynik = wynik * 1.7m;
            }
            else if (rbWysoka.Checked & rb3.Checked)
            {
                wynik = wynik * 1.8m;
            }
            else if (rbWysoka.Checked & rb4.Checked)
            {
                wynik = wynik * 2.0m;
            }
            else if (rbWysoka.Checked & rb7.Checked)
            {
                wynik = wynik * 2.2m;
            }



            if (tbWagaDocelowa.Text != "")
            {
                decimal roznica = Convert.ToDecimal(tbWagaDocelowa.Text) - Convert.ToDecimal(tbWaga.Text);
                if (roznica < 0)
                {
                    roznica = roznica * (-1);
                }

                wskaznik = zapotrzebowanie_podstawowe - wynik;
                if (wskaznik < 0)
                {
                    wskaznik = wskaznik * (-1);
                }

                decimal ilosc_kcl_z_wagi = roznica * 6000;
                decimal ilosc_dni_do_celu = ilosc_kcl_z_wagi / wskaznik;
                tbIloscDni.Text = Convert.ToString(Math.Round(ilosc_dni_do_celu,0, MidpointRounding.AwayFromZero));
            }

            tbDzienne.Text = Convert.ToString(wynik);
            
        }

        


        private void tbDzienne_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void tbIloscDni_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void rbMezczyzna_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
