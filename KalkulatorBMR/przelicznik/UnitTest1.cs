﻿using System;
using KalkulatorBMR;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;



namespace przelicznik
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string sWaga  = "12";
            decimal sprawdzenie = 12;
            decimal waga;

            if (sWaga != "")
            {
                waga = Convert.ToDecimal(sWaga);
            }
            else
            {
                waga = 0;
            }

            Assert.AreEqual(waga, sprawdzenie);
        }

       [TestMethod]
       public void TestMethod2()
        {
            decimal wzrost = 175;
            decimal waga = 92;
            decimal wiek = 30;
            decimal wynik = 1998.6m;
            decimal sprawdzenie;

            KalkulatorBMR.Form1 Testowanie = new KalkulatorBMR.Form1();
            sprawdzenie = Testowanie.Mezczyzna1(wzrost, waga, wiek);

            Assert.AreEqual(wynik, sprawdzenie);

        }
    }
}
